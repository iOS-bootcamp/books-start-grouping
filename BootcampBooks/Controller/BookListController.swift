//
//  ViewController.swift
//  BootcampBooks
//
//  Created by Keith Coughtrey on 26/04/17.
//  Copyright © 2017 NativeMobile. All rights reserved.
//

import UIKit
import SVProgressHUD

class BookListController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    var bookList: BookList?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.estimatedRowHeight = 100
        tableView.rowHeight = UITableViewAutomaticDimension
        
        bookList = BookRepositoryImpl.shared.list(query: "fishing")
    }

}

extension BookListController : UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return bookList?.items?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "BookCell", for: indexPath) as! BookCell
        if let book = bookList?.items?[indexPath.row] {
            cell.configure(book: book)
        }
        return cell
    }
    
}

extension BookListController : UITableViewDelegate {
    
}
