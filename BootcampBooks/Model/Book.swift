//
//  Book.swift
//  BootcampBooks
//
//  Created by Keith Coughtrey on 26/04/17.
//  Copyright © 2017 NativeMobile. All rights reserved.
//

import Foundation

struct Book {
    var title: String?
    var description: String?
    var thumbnailPath: String?
    var pageCount: Int?
    var publishedOn: String?

    var publishedYear: String? {
        if let published = publishedOn, published.characters.count > 3 {
            let index = published.index(published.startIndex, offsetBy: 4)
            return published.substring(to: index)
        }
        return nil
    }

}
